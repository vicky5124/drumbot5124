"""
This is a twitch bot.
It's used by `nitsuga5124` and `Aldusfrost`

Mainly created for drums song requests.
"""
import os
from datetime import datetime
import aiohttp
from twitchio.ext import commands
from youtube_dl import YoutubeDL

YOUTUBE_DL_OPTS = {
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'source_address': '0.0.0.0'
}

BOT = commands.Bot(
    irc_token=os.environ['TMI_TOKEN'],
    client_id=os.environ['CLIENT_ID'],
    nick=os.environ['BOT_NICK'],
    prefix=os.environ['BOT_PREFIX'],
    initial_channels=[os.environ['CHANNEL'], "nitsuga5124"]
)

HOOK_URL = os.environ['HOOK_URL']

async def get_json_request(url, twitch=False):
    if twitch:
        async with aiohttp.request(
                "GET",
                url,
                headers={
                    "Client-ID" : os.environ["CLIENT_ID"],
                    "User-Agent" : "Magic Browser",
                    "Authorization" : f"Bearer {os.environ['OAUTH']}"
                }) as request:

            if request.status == 200:
                return await request.json()

    else:
        async with aiohttp.request(
                "GET",
                url,
                headers={
                    'User-Agent' : "Magic Browser"
                }) as request:

            if request.status == 200:
                return await request.json()

async def post_request(title, url, user, thumbnail):
    data = f"""
{{"embeds": [{{
  "title": "{title}",
  "url": "{url}",
  "color": 16746239,
  "thumbnail": {{
    "url": "{thumbnail}"
  }},
  "author": {{
    "name": "{user}"
  }}
}}]}}"""

    async with aiohttp.request(
            "POST",
            HOOK_URL,
            headers={
                'User-Agent' : "Magic Browser",
                'Content-Type' : 'application/json'
            },
            #data='{"embeds": [{"title": "daba dee da bi daa", "color": 13770992}]}') as request:
            data=data) as request:
        if request.status == 204:
            print(f"{user} requested: {url} {title}")

async def add_to_queue(url, user):
    with YoutubeDL(YOUTUBE_DL_OPTS) as ydl:
        info_dict = ydl.extract_info(url, download=False)
        video_title = info_dict.get('title', None)
        thumbnail = info_dict.get('thumbnail', None)

    await post_request(video_title, url, user, thumbnail)

async def check_music_game(ctx, message=False):
    if message:
        data = await ctx.channel.get_stream()
    else:
        data = await ctx.get_stream()

    if data is not None:
        if data['game_id'] == "26936":
            return True
    return False

async def get_user(ctx, _message=False):
    #if message:
    #    data = await ctx.channel.get_stream()
    #else:
    #    data = await ctx.get_stream()

    #if data is not None:
    #    return data['user_name']
    #return None
    return ctx.channel.name

@BOT.event
async def event_ready():
    'Called once when the bot goes online and ready.'
    print(f"{os.environ['BOT_NICK']} is online!")

    #ws = BOT._ws
    #await ws.send_privmsg(os.environ['CHANNEL'], f"/me is ready to rock!")

@BOT.event
async def event_message(ctx):
    """
    Event that runs every time a message is sent in chat.
    Like on_message in d.py
    """
    # Ignores itself.
    if ctx.author.name.lower() == os.environ['BOT_NICK'].lower():
        return

    await BOT.handle_commands(ctx)

    user = await get_user(ctx, _message=True)
    if user is not None:
        if user.lower() == "aldusfrost":
            if await check_music_game(ctx, message=True):
                if "youtube.com" in ctx.content or "soundcloud.com" in ctx.content:
                    await add_to_queue(ctx.content, ctx.author.name)

@BOT.command()
async def test(ctx):
    'Test command `n!test`'
    await ctx.send('test passed!')
    stream = await ctx.get_stream()
    await ctx.send(stream)

@BOT.command()
async def uptime(ctx):
    'Gets the livestream uptime `n!uptime`'
    #streamer = ctx.channel.name
    #data = await get_json_request(f"https://api.twitch.tv/helix/streams?user_login={streamer}", twitch=True)
    data = await ctx.get_stream()
    if data is None:
        return await ctx.send("The stream is not live.")

    startup_time = data["started_at"]
    date_format = "%Y-%m-%dT%H:%M:%SZ"

    formatted_date = datetime.strptime(startup_time, date_format)
    current_time = datetime.utcnow()

    current_uptime = current_time - formatted_date
    await ctx.send(f"Uptime: {str(current_uptime).split('.')[0]}")

@BOT.command(aliases=['sr', 'songrequest'])
async def song_request(ctx, url=None):
    """
    For requesting songs to be played on drum streams.
    This command only works on Aldusfrost streams when he's playing
    "Music & Performing Arts"
    'n!song_request {YouTube/SoundClound url}'
    """
    user = await get_user(ctx)
    if user is not None:
        if user.lower() == "aldusfrost":
            #if await check_music_game(ctx):
                if url is not None:
                    await add_to_queue(url, ctx.author.name)
                else:
                    await ctx.send("Please, invoke the command with a YouTube or SoundClound URL after it.")

@BOT.command(aliases=['pyon'], name='ok')
async def _ok(ctx):
    '`n!ok`'
    await ctx.send("boomer")

@BOT.command(aliases=['skins'])
async def skin(ctx):
    'Posts the user osu!skin `n!skin`'
    user = await get_user(ctx)
    if user == "aldusfrost":
        await ctx.send("I change skins too often to upload them, usually it's a mix of a few. I'll gladly tell you on what they're based on")
    elif user == "nitsuga5124":
        await ctx.send("It's likely -GN's and Spro collab skin (boom 0524 edit)")

@BOT.command(aliases=['dcord'])
async def discord(ctx):
    'Posts the user\'s discord server `n!discord`'
    await ctx.send("https://discord.gg/EwBMnQy")

@BOT.command()
async def tablet(ctx):
    await ctx.send("/me Wacom Ctl 480")

@BOT.command(name="so")
async def _so(ctx, *, streamer):
    badges = ctx.author.badges
    if "moderator" in badges.keys() or "vip" in badges.keys():
        streamer = streamer.replace("@", "")
        await ctx.send(f"https://www.twitch.tv/{streamer}")


if __name__ == "__main__":
    BOT.run()
