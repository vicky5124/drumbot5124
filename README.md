### Running the bot for yourself:

`pipenv run python bot.py`

### Dependencies

You can get them installed with this command:
\
> `python -m pip install pipenv -U --user`

You will also need to setup a `.env` file with the next data:

```bash
TMI_TOKEN=oauth:
CLIENT_ID=
BOT_NICK=
BOT_PREFIX=n!
CHANNEL=
HOOK_URL=
```
\
Token: https://twitchapps.com/tmi/
\
Client: https://dev.twitch.tv/console/apps
\
Nick: The account name of the oAuth token comes from.
\
Prefix: w/e you want
\
Channel: The channel the bot will be on.
\
Hook: The webhook url you want the song requests to go to.

---

You will also need to remove `, "nitsuga5124"`
\
from the line 29, unless you want your bot to also be listening to my channel.

Changing every instance of `nitsuga5124` and `aldusfrost` though out the code to something of your like is also recommended in the case of hosting the bot for yourself.
